# 服务端协议开发示例

### 介绍
服务端扩展协议开发示例Demo


### 软件架构

![img.png](./doc/img.png)

| **字段** | **类型**  | **描述**        |
|:------:|---------|---------------|
|   Proc_Code    | 主键      | 主键            |
|   Proc_Module    | 驱动模块文件名 | 扩展协议文件名， 此处为：GWExProcDemo.STD.dll |
|   Proc_name    | 驱动名称    | 名称            |
|   Proc_parm    | 驱动启动参数  | 参数            |
|   Comment    | 描述 |             |

### 安装教程

将生成的文件复制到软件目录的dll下
![img.png](./doc/img-1.png)

### 使用说明

#### 初始化参数

```csharp
public bool init(GWExProcTableRow Row)
{
    var name = Row.Proc_name;
    var param = Row.Proc_parm;
    _inputArgs = $"扩展插件已加载，{name}，{param}";
    Console.WriteLine(_inputArgs);
    return true;
}
```

#### 系统控制项设置
```csharp
public void SetParm(string main_instruction, string minor_instruction, string value)
{
    Console.WriteLine($"调用事件：{main_instruction},{minor_instruction},{value}");
}
```