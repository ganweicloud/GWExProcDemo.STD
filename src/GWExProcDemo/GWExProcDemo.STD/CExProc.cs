﻿using GWDataCenter;
using GWDataCenter.Database;

namespace GWExProcDemo.STD;

public class CExProc : IExProcCmdHandle
{
    private string _inputArgs;
    public bool init(GWExProcTableRow Row)
    {
        var name = Row.Proc_name;
        var param = Row.Proc_parm;
        _inputArgs = $"扩展插件已加载，{name}，{param}";
        Console.WriteLine(_inputArgs);
        return true;
    }

    public void SetParm(string main_instruction, string minor_instruction, string value)
    {
        Console.WriteLine($"调用事件：{main_instruction},{minor_instruction},{value}");
    }
}